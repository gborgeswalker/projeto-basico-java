package br.sp.cursoselenium.page;


import br.sp.cursoselenium.core.BasePage;

public class HomePage extends BasePage {
	
	public String obterSaldoConta(String nome) {
	   return obtercelula("Conta", nome, "Saldo", "tabelaSaldo").getText();
	
	}

}
