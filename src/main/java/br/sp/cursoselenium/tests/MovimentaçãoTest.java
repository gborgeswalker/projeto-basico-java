package br.sp.cursoselenium.tests;


import org.junit.Test;
import org.junit.runners.MethodSorters;
import br.sp.cursoselenium.core.BaseTest;
import br.sp.cursoselenium.page.MenuPage;
import br.sp.cursoselenium.page.MovimentacaoPage;
import br.sp.cursoselenium.utils.DataUtils;

import static br.sp.cursoselenium.utils.DataUtils.obterDataFormatada;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;


public class Movimenta��oTest extends BaseTest {
	
	private MenuPage 			menuPage 	= new MenuPage();
	private MovimentacaoPage    movPage 	= new MovimentacaoPage();
	
	@Test
	public void test_1InserirMovimentacao() {
		menuPage.acessarTelaInserirMovimentacao();
		movPage.setDataMovimentacao(obterDataFormatada(new Date()));
		movPage.setDataPagamento(obterDataFormatada(new Date()));
		movPage.setDescricao("Movimenta��o do Teste");
		movPage.setInteressado("Interessado Qualquer");
		movPage.setValor("500");
		movPage.setConta("Conta para movimentacoes");
		movPage.setStatusPago();
		movPage.salvar();
		Assert.assertEquals("Movimenta��o adicionada com sucesso!", movPage.obterMensagemSucesso());
	}
	
	@Test
	public void test_2CamposObrigatorios() {
		menuPage.acessarTelaInserirMovimentacao();
		movPage.salvar();
		List<String> erros = movPage.obterErros();
		
		//Assert.assertEquals("Data da Movimenta��o � obrigat�rio", erros.get(0)); -> Iria pegar pela posi��o
		//Assert.assertTrue(erros.contains("Data da Movimenta��o � obrigat�rio")); -> Usando Contaiins
		Assert.assertTrue(erros.containsAll(Arrays.asList(
				"Data da Movimenta��o � obrigat�rio", "Data do pagamento � obrigat�rio",
				"Descri��o � obrigat�rio", "Interessado � obrigat�rio",
				"Valor � obrigat�rio", "Valor deve ser um n�mero")));
		Assert.assertEquals(6, erros.size());// chega a quantidade de erros
	}

	@Test
	public void test_3MovimentacaoFuruta() {
		menuPage.acessarTelaInserirMovimentacao();
		
		Date dataFutura = DataUtils.obterDataComDiferencaDias(5); // diferen�a de 5 dias
		movPage.setDataMovimentacao(DataUtils.obterDataFormatada(dataFutura));
		movPage.setDataPagamento(obterDataFormatada(dataFutura));
		movPage.setDescricao("Movimenta��o do Teste");
		movPage.setInteressado("Interessado Qualquer");
		movPage.setValor("500");
		movPage.setConta("Conta para movimentacoes");
		movPage.setStatusPago();
		movPage.salvar();
		List<String> erros = movPage.obterErros();
		Assert.assertTrue(erros.contains("Data da Movimenta��o deve ser menor ou igual � data atual"));
		Assert.assertEquals(1, erros.size());// chega a quantidade de erros
	}
}
