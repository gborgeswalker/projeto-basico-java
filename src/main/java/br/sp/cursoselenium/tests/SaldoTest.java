package br.sp.cursoselenium.tests;

import org.junit.Assert;
import org.junit.Test;

import br.sp.cursoselenium.core.BaseTest;
import br.sp.cursoselenium.page.HomePage;
import br.sp.cursoselenium.page.MenuPage;

public class SaldoTest extends BaseTest {
	
	HomePage page = new HomePage();
	MenuPage menu = new MenuPage();
	
	@Test
	public void testSaldoConta() {
		menu.acessarTelaPrincipal();
		Assert.assertEquals("534.00", page.obterSaldoConta("Conta para saldo"));
		
	}
	

}