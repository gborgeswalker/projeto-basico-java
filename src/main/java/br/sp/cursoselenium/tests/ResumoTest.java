package br.sp.cursoselenium.tests;

import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import br.sp.cursoselenium.core.BaseTest;
import br.sp.cursoselenium.core.DriverFactory;
import br.sp.cursoselenium.page.MenuPage;
import br.sp.cursoselenium.page.ResumoPage;

import static br.sp.cursoselenium.core.DriverFactory.getDriver;

import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;


public class ResumoTest extends BaseTest {
	
	private MenuPage  	menuPage   = new MenuPage();
	private ResumoPage  resumoPage = new ResumoPage();
	
	@Test
	public void  test_1ExcluirMovimentacao() {
		menuPage.acessarTelaResumo();
		resumoPage.excluirMovimentacao();

		Assert.assertEquals("Movimenta��o removida com sucesso!", resumoPage.obterMensagemSucesso());		
	}
	
	
	@Test
	public void  test_2ResumoMensal() {
		menuPage.acessarTelaResumo();
		Assert.assertEquals("Seu Barriga - Extrato", getDriver().getTitle());	
		
		resumoPage.selecionarAno("2019");
		resumoPage.buscar();
		
		try { // com o try, caso n�o ache o elemento, no cath trata a excess�o como sucesso
			DriverFactory.getDriver().findElement(By.xpath("//*[@id='tabelaExtrato']/tbody/tr"));
			Assert.fail(); // caso ache, aqui far� o teste falhar
		} catch (NoSuchElementException e) {
			
		}
		
	}
	
	@Test
	public void  test_3ResumoMensal() { // Outra forma de fazer o mesmo teste acima
		menuPage.acessarTelaResumo();
		Assert.assertEquals("Seu Barriga - Extrato", getDriver().getTitle());	
		
		resumoPage.selecionarAno("2019");
		resumoPage.buscar();
		
		List<WebElement> elementosEncontrados =
				DriverFactory.getDriver().findElements(By.xpath("//*[@id='tabelaExtrato']/tbody/tr"));
		Assert.assertEquals(0, elementosEncontrados.size());// est� validando que n�o vou encontrar nada
	
	}
}
