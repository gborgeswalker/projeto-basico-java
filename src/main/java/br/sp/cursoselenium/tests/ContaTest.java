package br.sp.cursoselenium.tests;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import br.sp.cursoselenium.core.BaseTest;
import br.sp.cursoselenium.page.ContasPage;
import br.sp.cursoselenium.page.MenuPage;


public class ContaTest extends BaseTest {
	
	MenuPage 	menuPage 	= new MenuPage();
	ContasPage 	contasPage  = new ContasPage();

	@Test
	public void test_1InserirConta() {
		menuPage.acessarTelaInserirConta();
		contasPage.setNome("Conta do Teste");
		contasPage.salvar();
		Assert.assertEquals("Conta adicionada com sucesso!", contasPage.obterMensagemSucesso());
		
	}
	
	@Test
	public void test_2AlterarConta() {
		menuPage.acessarTelaListarConta();
		contasPage.clicarAlterarConta("Conta para alterar");
		contasPage.setNome("Conta alterada");
		contasPage.salvar();
		Assert.assertEquals("Conta alterada com sucesso!", contasPage.obterMensagemSucesso());
	
	}
	
	@Test
	public void test_3InserirContaMesmoNOme() {
		menuPage.acessarTelaInserirConta();
		contasPage.setNome("Conta mesmo nome");
		contasPage.salvar();
		Assert.assertEquals("J� existe uma conta com esse nome!", contasPage.obterMensagemErro());
		
	}
	
	
}
