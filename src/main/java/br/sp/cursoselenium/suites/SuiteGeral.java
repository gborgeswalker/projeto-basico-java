package br.sp.cursoselenium.suites;



import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.sp.cursoselenium.core.DriverFactory;
import br.sp.cursoselenium.page.LoginPage;
import br.sp.cursoselenium.tests.ContaTest;
import br.sp.cursoselenium.tests.MovimentaçãoTest;
import br.sp.cursoselenium.tests.RemoverMovimentacaoContaTest;
import br.sp.cursoselenium.tests.ResumoTest;
import br.sp.cursoselenium.tests.SaldoTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	ContaTest.class,
	MovimentaçãoTest.class,
	RemoverMovimentacaoContaTest.class,
	SaldoTest.class,
	ResumoTest.class
})
public class SuiteGeral {
	private static LoginPage page = new LoginPage();
	
	@BeforeClass
	public static void reset() {
		page.acessarTelaInicial();
		page.setEmail("gborges@gmail.com");
		page.setSenha("pwd123");
		page.entrar();
		page.resetar();
		
		DriverFactory.killDriver();
	}
	

}
