package br.sp.cursoselenium.core;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import br.sp.cursoselenium.core.Propriedades.TipoExecucao;

public class DriverFactory {
	
//	private static WebDriver driver;
	private static ThreadLocal<WebDriver>  threadDriver = new ThreadLocal<WebDriver>() {
		@Override
		protected synchronized WebDriver initialValue() {
			return initDriver();
		}
	};

	private DriverFactory() {}
	public static WebDriver getDriver() { // quando pedir um driver, quem vai retornar � a thread
		return threadDriver.get();
	}
	
	public static WebDriver initDriver() {
		WebDriver driver = null;
		if(Propriedades.TIPO_EXECUCAO == TipoExecucao.LOCAL) { // Se o tipo de execu��o for LOCAL, segue o padr�o.
			switch (Propriedades.BROWSER) { // definindo qual browser usar
				case FIREFOX: driver = new FirefoxDriver(); break;
				case CHROME: driver = new ChromeDriver(); break;
			}
		}
		if(Propriedades.TIPO_EXECUCAO == TipoExecucao.GRID) { 
			DesiredCapabilities cap = null;
				switch (Propriedades.BROWSER) {
				case FIREFOX: cap = DesiredCapabilities.firefox(); break;
				case CHROME: cap = DesiredCapabilities.chrome(); break;

				default:
					break;
				}
				try {
					driver = new RemoteWebDriver(new URL("http://192.168.1.14:4444/wd/hub"), cap);
				} catch (MalformedURLException e) {
					System.err.println("Falha na conex�o com o GRID");
					e.printStackTrace();
				}
		}
		if(Propriedades.TIPO_EXECUCAO == TipoExecucao.NUVEM) { 
			DesiredCapabilities cap = null;
				switch (Propriedades.BROWSER) {
				case FIREFOX: cap = DesiredCapabilities.firefox(); break;
				case CHROME: cap = DesiredCapabilities.chrome(); break;

				default:
					break;
				}
				try {
					driver = new RemoteWebDriver(new URL("http://ondemand.saucelabs.com:80/wd/hub"), cap);
				} catch (MalformedURLException e) {
					System.err.println("Falha na conex�o com o GRID");
					e.printStackTrace();
				}
		}
			
		driver.manage().window().setSize(new Dimension(1200, 765));
		return driver;
	}
	
	public static void killDriver() { // vai matar o driver, e far� o driver ser nulo, para quando o driver for chamado de novo
		WebDriver driver = getDriver();
		if(driver != null) { 
			driver.quit();
			driver = null;
		}
		if(threadDriver != null) {
			threadDriver.remove();
		}
	}
}

