package br.sp.cursoselenium.core;
import static br.sp.cursoselenium.core.DriverFactory.getDriver;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import static br.sp.cursoselenium.core.DriverFactory.killDriver;
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import br.sp.cursoselenium.page.LoginPage;

public class BaseTest {
	
	@Rule
	public TestName testName = new TestName();  // D� o nome do teste a screenshot
	
	private  LoginPage page = new LoginPage();

	@Before
	public void inicializa() {
		page.acessarTelaInicial();
		page.setEmail("gborges@gmail.com");
		page.setSenha("pwd123");
		page.entrar();
	}
	
	@After
	public void finaliza() throws IOException {
		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File("target" + File.separator + "screenshot/" +  //File.separator � pra determinar a barra "/" que o sistema estiver usando
				File.separator + testName.getMethodName() + ".jpg")); // D� o nome do teste a screenshot
		
		if(Propriedades.FECHAR_BROWSER) {
		killDriver();
		}
	}
}
