package br.sp.cursoselenium.core;

public class Propriedades {
	
	public static boolean FECHAR_BROWSER = true; // n�o deixa o browser fechar ao final do teste (caso seja FALSE)

	public static Browsers BROWSER = Browsers.CHROME;
	
	public static TipoExecucao TIPO_EXECUCAO = TipoExecucao.GRID;
	
	public enum Browsers {
		CHROME,
		FIREFOX
	}
	
	public enum TipoExecucao{
		LOCAL,
		GRID,
		NUVEM
	}
}
